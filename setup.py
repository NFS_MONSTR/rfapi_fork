import setuptools
import rfapi

description = open("README.md", "r").read()
setuptools.setup(
    name="rfapi",
    version=rfapi.__version__,
    author="Кацков Павел",
    author_email="pasha_kackov@mail.ru",
    long_description=description,
    long_description_content_type="text/markdown",
    url="https://srv.nppsatek.ru:42917/ichega/RFAPI",
    packages=setuptools.find_packages(),
)