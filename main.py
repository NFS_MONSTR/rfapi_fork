from rfapi.methodology import Methodology
from rfapi import kv as rf_kv
from rfapi.backend import backend as rf_backend
from rfapi import actions as rf_actions
from tornado import template
import os
import asyncio
import  ujson

import aiohttp
from bs4 import BeautifulSoup
from rfapi.backend import user as _rf_user



class TestMethodology(Methodology):
    """
    Методология собирает данные о всех задачах на карте и выводит следующую информацию:
    1. Всего количество задач
    2. Количество задач по статусам
    3. Процент от выполненного плана
    """
    def __init__(self, dirname=os.getcwd()):
        super().__init__(dirname)
        self.info = self.load_info(dirname)
        
    async def verify(self,debug, username, password, map_id, types_info=None):

        return False

    async def init(self, username, password, map_id, status_info, debug):
        await super().init(username, password, map_id, status_info, debug)






    async def run(self):

        """
        Совершает какую-то полезную работу
        :return:
        """
        #html = "<style>\n"
        #html += open("github-markdown.css", "r").read()
        #html += "</style>"
        #html += open("temp.html", "r").read()
        #await self.actions.node_user_property_create("eaab955f-a50d-4a99-9125-43d8ac6160d9", 6, "MD документ", html)
        #await self.backend.authentication("pasha_kackov@mail.ru", "4014SqRt2")
        #map_infp = await self.backend.get_maps_node_info("569bab00-114f-4005-834d-07471642cb50", "799c2065-23ab-4ad3-ab53-94a9c73defb4")
        #map_infp = await self.backend.get_maps_node_info("569bab00-114f-4005-834d-07471642cb50", "799c2065-23ab-4ad3-ab53-94a9c73defb4")
        #print(map_infp)
        #http://188.68.16.188/#mindmap?mapid=569bab00-114f-4005-834d-07471642cb50&nodeid=799c2065-23ab-4ad3-ab53-94a9c73defb4
        mi = await self.actions.map_create("Hello")
        print(mi)
        r = await self.actions.node_create_child(mi["id"],mi["root_node_id"],0,"test node")
        print(r)



        pass

    async def done(self):
        """
        Завершает работу методологии
        :return: Результат работы методологии, если есть
        """
        await self.backend.logout()
        return ""


    async def start(self, username, password, map_id, status_info, debug):
        """
        Запускает методологию
        :param username:
        :param password:
        :param map_id:
        :param info:
        :param debug:
        :return:
        """
        await self.init(username, password, map_id, status_info, debug)
        await self.run()
        return await self.done()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    methodology = TestMethodology("")
    print("start")
    config = ujson.loads(open("config.json", "r").read())

    loop.run_until_complete(methodology.start("pasha_kackov@mail.ru", config["password"], "569bab00-114f-4005-834d-07471642cb50", {"status": "run"}, debug=True))

    print("end")