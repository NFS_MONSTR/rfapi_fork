from . import kv as rf_kv
import os
import asyncio
from .methodology import Methodology
import traceback, sys

class PassiveMethodology(Methodology):
    def __init__(self, dirname=os.getcwd()):
        super().__init__(dirname)
    async def init(self, username, password, map_id, status_info, debug, settings):
        """
        Функция инициализирует методологию и все что с ней связано
        :param username:
        :param password:
        :param map_id:
        :param info:
        :param debug:
        :param access_list: Список пользователей (id пользователей), на которых работает методология
        :return:
        """
        try:
            #print("PassiveMethodology.init: start")
            self.settings = settings
            await super().init(username, password, map_id, status_info, debug)
            self.kv = rf_kv.KVListener(self.backend)
            #print("KV ", self.kv)
            await self.kv.init(self.map_id)
            #print("KVi ", self.kv)
            self.NotifLast = await self.kv.get_notif_last()
            #print("NL ", self.NotifLast)
        except Exception as e:
            print("PassiveMathodology. Ошибка инициализации  методологии", e)
        #print("PassiveMethodology.init: end")

    async def run(self):
        """
        Следит за NotifLast и забирает данные о произошедших событиях
        Когда информация о событии приходит, отдает ее на обработку в Handle(event)
        :return:
        """
        await super().run()
        print("Start handler")
        while True:
            try:
                # Ждем событие в течении 60 секунд
                print("Ожидание события...")
                #Пока статус "run" - работает...
                if self.status_info["status"] != "run":
                    #print(self.status_info)
                    break

                notif_last_info = await self.kv.get_notif_last(timeout=10)
                if ( notif_last_info == None):
                    print("Ничего не произошло")
                    continue
                #Получаем список информации о произошедших событиях
                events = await self.kv.get_event_list()
                #Обработка событий
                for event in events:
                    # Проверка, является ли совершивший событие в списке разрешенных
                    if "value" in event:
                        if "who" in event["value"]:
                            if "id" in event["value"]["who"]:
                                if not (event["value"]["who"]["id"] in self.settings["access_list"]):
                                    print(event["value"]["who"]["id"], " отсутствует в ", self.settings["access_list"])
                                    break
                                else:
                                    print(event["value"]["who"]["id"], " есть в ", self.settings["access_list"])
                                    await self.handle(event)

                        else:
                            break
                    else:
                        break


            except Exception as e:
                print("Error: ", e)
                #self.status_info["status"] = "error"
                #self.status_info["error"] = e
                continue
        print("Start awaiting")
        while True:
            await  asyncio.sleep(1)
            if self.status_info["status"] == "stop":
                break

    async def done(self):
        """
        Завершает работу методологии
        :return:
        """
        await super().done()


    async def handle(self, event):
        """
        Обрабатывает событие
        :param event: информация о событии
        :return:
        """
        pass

    async def verify(self,debug, username, password, map_id, types_info=None):
        """
        Функция проверяет карту на соответствие требованиям методологии:
        Наличие нужных типов, наличие полей у типов и т.п.
        :param map_id: id карты для проверки
        :return: True - если методология применима к карте, False - не применима
        """

        return False

    async def start(self, username, password, map_id, status_info, debug, settings):
        """
        Запускает методологию
        :param username:
        :param password:
        :param map_id:
        :param info:
        :param debug:
        :param access_list: Список пользователей, на которых работает методология
        :return:
        """
        #print("INIT")
        await self.init(username, password, map_id, status_info, debug, settings)
        #print("RUN")
        await self.run()
        #print("DONE")
        return await self.done()