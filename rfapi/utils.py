"""
Модуль содержит полезные для работы с RedForester классы и функции
"""
from time import time

def get_timestamp():
    """
    Получаем текущий timestamp (время unix в миллисекундах)
    :return:
    """
    return int(time() * 1000)
