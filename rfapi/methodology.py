from rfapi.backend import backend as rf_backend
from . import actions as rf_actions
import ujson
import os
class Methodology():
    def __init__(self, dirname=os.getcwd()):
        self.info = self.load_info(dirname)

    async def init(self, username, password, map_id, status_info, debug):
        """
        Функция инициализирует методологию и все что с ней связано
        :param username:
        :param password:
        :param map_id:
        :param info:
        :param debug:
        :return:
        """
        try:
            #print("Methodology.init: start")
            self.debug = debug
            self.backend = rf_backend.RFBackend(debug=debug)
            await self.backend.login(username, password)
            self.actions = rf_actions.Actions(self.backend)
            self.map_id = map_id
            self.status_info = status_info
        except Exception as e:
            print("Mathodology .Ошибка инициализации  методологии", e)
        #print("Methodology.init: end")


    def load_info(self, dirname=os.getcwd()):
        """
        Загрузка информации о методологии
        :return:
        """

        config = ujson.loads(open(os.path.join(dirname, "config.json"), "r").read())
        return {
            "name": config["name"],
            "version": config["version"],
            "author": config["author"],
            "do": config["do"],
            "requirements": config["requirements"],
        }

    def information(self):
        """
        Возвращает информацию о методологии:
        1. Имя
        2. Версия
        3. Автор
        4. Что делает
        5. Требования к методологии
        :return:
        """
        return self.info

    async def run(self):
        """
        Совершает какую-то полезную работу
        :return:
        """
        pass

    async def done(self):
        """
        Завершает работу методологии
        :return: Результат работы методологии, если есть
        """
        await  self.backend.logout()

    async def verify(self, debug, username, password, map_id):
        """
        Функция проверяет карту на соответствие требованиям методологии:
        Наличие нужных типов, наличие полей у типов и т.п.
        :param map_id: id карты для проверки
        :return: True - если методология применима к карте, False - не применима
        """
        return False

    async def start(self, username, password, map_id, status_info, debug):
        """
        Запускает методологию
        :param username:
        :param password:
        :param map_id:
        :param info:
        :param debug:
        :return:
        """
        await self.init(username, password, map_id, status_info, debug)
        await self.run()
        return await self.done()
