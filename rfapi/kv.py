import ujson
from aiohttp import ClientSession
from rfapi.backend import backend as rf_backend
from . import utils

class KVListener():
    """
    Предназначен для прослушки сервера kv и
    получения информации о событиях
    """
    def __init__(self, backend: rf_backend.RFBackend):
        self.backend = backend
        self.config = {
            "NotifLast":{
                "version": 0,
                "value": 0,
            }
        }
    async def init(self, map_id):
        self.config["kv_server_info"] = await self.backend.get_kv_server_info()
        self.config["rf_sid"] = await self.backend.get_rf_sid()
        self.config["kv_session"] = await self.backend.get_kv_session()
        self.config["map_id"] = map_id

    async def get_notif_last(self, timeout=60):
        """
        Возвращает данные о последнем событии - NotifLast (номер версии и timestamp)
        :param map_id: id карты
        :return: NotifLast (номер версии и timestamp)
        """
        async with ClientSession() as session:
            req = '{}://{}:{}/v2/kv/keys/RF:{}:mapNotifLast:{}:{}?n=3&r=2&w=2&waitTimeout={}&waitVersion={}'.format(
                            self.config["kv_server_info"]["protocol"], self.config["kv_server_info"]["host"],
                            self.config["kv_server_info"]["port"],
                            self.config["rf_sid"], self.config["map_id"], self.config["kv_session"],
                            timeout, self.config["NotifLast"]["version"],
                    )
            #print(req)
            async with session.get(req) as response:
                if (response.status != 200):
                    return None
                data = await response.text()
                data = ujson.loads(data)
                self.config["OldNotifLast"] = self.config["NotifLast"]
                self.config["NotifLast"] = data


        return data

    async def get_event_list(self):
        """
        Возвращает список событий, которые произошли
        с последней известной версии NotifLast
        :return: список событий
        """
        async with ClientSession() as session:
            async with session.get(
                    '{}://{}:{}/v2/kv/partition/RF:{}:mapNotif:{}:{}?n=3&r=2&w=2&from={}&to={}'.format(
                        self.config["kv_server_info"]["protocol"], self.config["kv_server_info"]["host"],
                        self.config["kv_server_info"]["port"],
                        self.config["rf_sid"], self.config["map_id"], self.config["kv_session"],
                        self.config["OldNotifLast"]["value"], utils.get_timestamp(),
                    )) as response:
                if response.status == 200:
                    event_list = await response.text()
                    event_list = ujson.loads(event_list)
                    return event_list
                else:
                    return None


