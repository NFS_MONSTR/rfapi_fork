"""
Модуль реализует работу с backend RedForester
"""

from hashlib import md5
from aiohttp import ClientSession, BasicAuth
import asyncio
import ujson
#получаем доступ к циклу
try:
    loop = asyncio.get_event_loop()
except RuntimeError:
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

CONFIG_BASE_URL_DEBUG = "http://188.68.16.188:80"
CONFIG_BASE_URL_NO_DEBUG = "http://app.redforester.com:80"


class RFBackend():
    """
    Данный класс нужен для работы с backend сервером RedForester
    """
    def __init__(self, debug=False):
        """
        Задает базовые параметры конфигурации, такие как хост адрес backend сервера
        :param debug: Определяет какой сервер использовать - тестовый или основной
        """
        self.debug = debug
        self.config = {}
        if (debug):
            self.config["base_url"] = CONFIG_BASE_URL_DEBUG
        else:
            self.config["base_url"] = CONFIG_BASE_URL_NO_DEBUG
        self.config["username"] = "None"
        self.config["password_hash"] = "None"
        self.session = None
        self.auth = False


    def backend_auth(method):
        def wrapper(self, *args, **kwargs):
            assert self.auth == True
            return method(self, *args, **kwargs)

        return wrapper


    @staticmethod
    def __get_password_hash(password):
        """
        Возвращает md5 хеш пароля
        :param password: Пароль пользователя в неизменном виде
        :return: Хеш пароля
        """

        return  md5(password.encode('utf-8')).hexdigest()


    @backend_auth
    def get_cookie(self):
        return str(self.session.cookie_jar.filter_cookies('http://app.redforester.com')['session'])[20:]


    async def cookie_login(self, cookie):
        """
        Производит авторизацию пользователя в backend RedForester-а по куки от прошлой сессии
        :param cookie: куки для aiohttp
        :return:
        """
        try:
            self.session = ClientSession(cookies={'session': cookie})
            if (self.debug):
                url = CONFIG_BASE_URL_DEBUG
            else:
                url = CONFIG_BASE_URL_NO_DEBUG
            url = url + "/api/user"
            async with self.session.get(url) as response:
                assert response.status == 200
        except Exception as e:
            print("Ошибка: ", e)
            self.session = None
            self.auth = False
        else:
        #    self.config["username"] = username
        #    self.config["password_hash"] = self.__get_password_hash(password)
            self.auth = True
        finally:
            return self.auth


    async def login(self, username, password):
        """
        Производит авторизацию пользователя в backend RedForester-а
        :param username: Имя пользователя
        :param password: Пароль
        :return: True - если пользователя авторизован, False - если нет
        """
        password_hash = RFBackend.__get_password_hash(password)

        try:
            self.session = ClientSession(auth=BasicAuth(login=username, password=password_hash))
            if (self.debug):
                url = CONFIG_BASE_URL_DEBUG
            else:
                url = CONFIG_BASE_URL_NO_DEBUG
            url = url + "/api/user"
            async with self.session.get(url) as response:
                assert response.status == 200
        except Exception as e:
            print("Ошибка: ", e)
            self.session = None
            self.auth = False
        else:
            self.config["username"] = username
            self.config["password_hash"] = self.__get_password_hash(password)
            self.auth = True
        finally:
            return self.auth

    @staticmethod
    async def authentication( username, password, debug=False):
        """
        Производит авторизацию пользователя в backend RedForester-а
        По сути проверка, является ли корректными данные пользователя
        :param username: Имя пользователя
        :param password: Пароль
        :return: True - если пользователя корректен, False - если нет
        """
        password_hash = RFBackend.__get_password_hash(password)
        if (debug):
            url = CONFIG_BASE_URL_DEBUG
        else:
            url = CONFIG_BASE_URL_NO_DEBUG
        url = url + "/api/user"

        try:
            session = ClientSession(auth=BasicAuth(login=username, password=password_hash))

            async with session.get(url) as response:

                return response.status == 200
        except Exception as e:
            print("Ошибка: ", e)
            return False

        finally:
            await session.close()


    @backend_auth
    async def logout(self):
        """
        Произвоит закрытие сессии пользователя
        :return:
        """

        await self.session.close()
        self.session = None
        self.auth = False

    @backend_auth
    async def get_kv_server_info(self):
        """
        Возвращает данные о сервере kv, а именно:
        1. Хост адрес
        2. Порт
        3. Протокол
        :return: Словарь с данными о сервере kv
        """
        try:
            async with self.session.get('{}/api/server/kv'.format(self.config["base_url"])) as response:
                kv = await response.read()
        except Exception as e:
            print("Ошибка: ", e)
        else:
            return ujson.loads(kv)

    @backend_auth
    async def get_rf_sid(self):
        """
        Возвращает SID RedForster
        :return: SID RedForster
        """
        try:
            async with self.session.get('{}/api/server/sid'.format(self.config["base_url"])) as response:
                rf_sid = await response.read()
        except Exception as e:
            print("Ошибка: ", e)
        else:
            return str(ujson.loads(rf_sid))

    @backend_auth
    async def get_kv_session(self):
        """
        Возвращает kv_session конкретного пользователя, он необходим для совершения запросов в kv
        :return: kv_session пользователя
        """
        try:
            async with self.session.get('{}/api/user'.format(self.config["base_url"])) as response:
                user_info = await response.read()
        except Exception as e:
            print("Ошибка: ", e)
        else:
            user_info = ujson.loads(user_info)
            return str(user_info["kv_session"])

    @backend_auth
    async def get_user_info(self):
        """
        Возвращает информацию о пользователе
        :return: словарь данных пользователя
        """
        try:
            async with self.session.get('{}/api/user'.format(self.config["base_url"])) as response:
                user_info = await response.read()
        except Exception as e:
            print("Ошибка: ", e)
        else:
            user_info = ujson.loads(user_info)
            return user_info

    @backend_auth
    async def get_maps_info(self):
        """
        Возвращает информацию о картах пользователя
        :return: список данных карт пользователя
        """
        try:
            async with self.session.get('{}/api/maps'.format(self.config["base_url"])) as response:
                maps_info = await response.read()
        except Exception as e:
            print("Ошибка: ", e)
        else:
            maps_info = ujson.loads(maps_info)
            return maps_info

    @backend_auth
    async def get_map_info(self, map_id):
        """
        Возвращает информацию о карте пользователя с id = map_id
        :return: список данных карты
        """
        try:
            async with self.session.get('{}/api/maps/{}'.format(self.config["base_url"], map_id)) as response:
                map_info = await response.read()
        except Exception as e:
            print("Ошибка: ", e)
        else:
            map_info = ujson.loads(map_info)
            return map_info

    @backend_auth
    async def get_map_id(self, title):
        """
        Возвращает id карты с заданым заголовком - title
        :param title: заголовок карты
        :return: id карты с заголовком title
        """
        try:
            async with self.session.get('{}/api/maps'.format(self.config["base_url"])) as response:
                maps_list = await response.read()
        except Exception as e:
            print("Ошибка: ", e)
            return None
        else:
            maps_list = ujson.loads(maps_list)
            for map in maps_list:
                if map["name"] == title:
                    return map["id"]
            return None

    @backend_auth
    async def get_map_users(self, map_id):
        """
        Возвращает список пользователей карты
        :param map_id: id карты
        :return: список пользователей
        """
        try:
            async with self.session.get('{}/api/maps/{}/users'.format(self.config["base_url"], map_id)) as response:
                user_list_str = await response.read()
        except Exception as e:
            print("Ошибка: ", e)
            return None
        else:
            user_list = ujson.loads(user_list_str)
            return user_list


    @backend_auth
    async def get_types_info(self, map_id):
        """
        Возвращает информацию о типах узлов
        :return: список данных о типах узлов
        """
        try:
            async with self.session.get('{}/api/maps/{}/node_types'.format(self.config["base_url"], map_id)) as response:
                types_info = await response.read()
        except Exception as e:
            print("Ошибка: ", e)
        else:
            types_info = ujson.loads(types_info)
            return types_info

    @backend_auth
    async def get_type_info(self, map_id, type_id):
        """
        Возвращает информацию о типе узла
        :return: данные типа узла
        """
        try:
            types_info = await self.get_types_info(map_id)
            for type in types_info:
                if str(type["id"]) == type_id:
                    type_info = type
                    break
        except Exception as e:
            print("Ошибка: ", e)
        else:
            return type_info

    @backend_auth
    async def get_type_name_by_id(self, map_id, type_id):
        """
        Возвращает имя типа
        :param map_id:  id карты
        :param type_id: id типа
        :return: имя типа
        """
        try:
            types_info = await self.get_types_info(map_id)
            for type in types_info:
                if str(type["id"]) == type_id:
                    type_info = type
                    break
        except Exception as e:
            print("Ошибка: ", e)
        else:
            return type_info["name"]

    @backend_auth
    async def get_type_id_by_name(self, map_id, name):
        """
        Возвращает имя типа
        :param map_id:  id карты
        :param type_id: id типа
        :return: имя типа
        """
        try:
            types_info = await self.get_types_info(map_id)
            for type in types_info:
                if str(type["name"]) == name:
                    type_info = type
                    break
        except Exception as e:
            print("Ошибка: ", e)
        else:
            return type_info["id"]

    @backend_auth
    async def get_nodes_info(self, map_id):
        """
        Возвращает информацию о узлах
        :return: список данных о узлах
        """
        try:
            async with self.session.get('{}/api/maps/{}/nodes'.format(self.config["base_url"], map_id)) as response:
                nodes_info = await response.read()
        except Exception as e:
            print("Ошибка: ", e)
        else:
            nodes_info = ujson.loads(nodes_info)
            return nodes_info

    @backend_auth
    async def get_node_info(self, node_id):
        """
        Возвращает информацию о узле
        :return: данные о узле
        """
        try:
            async with self.session.get('{}/api/nodes/{}'.format(self.config["base_url"], node_id)) as response:
                node_info = await response.read()
                #print(node_info)
        except Exception as e:
            print("Ошибка: ", e)
        else:

            node_info = ujson.loads(node_info)
            return node_info

    @backend_auth
    async def get_maps_node_info(self, map_id, node_id):
        """
        Возвращает информацию о узле
        :return: данные о узле
        """
        try:
            async with self.session.get('{}/api/maps/{}/nodes/{}'.format(self.config["base_url"], map_id, node_id)) as response:
                node_info = await response.read()
                #print(node_info)
        except Exception as e:
            print("Ошибка: ", e)
        else:

            node_info = ujson.loads(node_info)
            return node_info

    @backend_auth
    async def send_change_request(self, url, method, body):
        """
        Создает и посылает запрос на изменение через RedForester API
        :param url: - url куда посылется запрос на изменение (url берется из API)
        :param method: - метод запроса (обычно POST or PATCH)
        :param data: - данные с изменениями
        :return: результат запроса (статус ответа + текст ответа)
        """
        try:
            async with self.session.request(
                    method,
                    self.config["base_url"] + url,
                    data=ujson.dumps(body),
                    headers={
                        "Content-Type": "application/json; charset=UTF-8"
                    }
            ) as response:
                return response.status, await response.text()
                pass
        except Exception as e:
            print("Ошибка: ", e)



