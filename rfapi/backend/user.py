from . import backend
import ujson
import asyncio
import datetime
class User():
    """
    Обертка, позволяющая удобно получать информацию о пользователе
    Перед использованием необходимо вызвать update()
    """
    def __init__(self, backend):
        self.backend = backend

    def id(self):
        """
        Возвращает id пользователя
        :return: id пользователя
        """
        return self.info["user_id"]

    def username(self):
        """
        Возвращает логин пользователя
        :return: логин пользователя
        """
        return self.info["username"]

    def name(self):
        """
        Возвращает имя пользователя
        :return: имя пользователя
        """
        return self.info["name"]

    def surname(self):
        """
        Возвращает фамилию пользователя
        :return: фамилия пользователя
        """
        return self.info["surname"]

    def avatar_url(self):
        """
        Возвращает url на аватар пользователя
        :return: url
        """
        return self.info["avatar"]

    def date_of_registry(self, is_timestamp=False):
        """
        Возвращает дату регистрации
        Вид зависит от параметра is_timestrap
        :param is_timestamp: если True, то выведет в виде миллисекунд,
            False - в читабельном формате
        :return: дата регистрации
        """
        if is_timestamp:
            return self.info["registration_date"]
        else:
            return datetime.datetime.fromtimestamp(self.info["registration_date"]).strftime('%Y-%m-%d %H:%M:%S')


    def birthday(self, is_timestamp=False):
        """
        Возвращает дату рождения
        Вид зависит от параметра is_timestrap
        :param is_timestamp: если True, то выведет в виде миллисекунд,
            False - в читабельном формате
        :return: дата рождения
        """
        if is_timestamp:
            return self.info["birthday"]
        else:
            return datetime.datetime.fromtimestamp(self.info["birthday"]).strftime('%Y-%m-%d %H:%M:%S')

    async def update(self):
        """
        Обновляет информацию о пользователе (лезет на сервер)
        :return:
        """
        self.info = await self.backend.get_user_info()




