"""
    Предназначен для совершения действий с картой в RedForester
"""
from rfapi.backend import backend
import ujson


class Actions():
    """
    Предназначен для совершения действий с картой в RedForester
    """

    def __init__(self, rfbackend: backend.RFBackend):
        self.backend = rfbackend

    async def node_change_global_title(self, node_id, title):
        """
        Меняет заголовок узла
        :return: статус ответа и текст ответа
        """
        try:
            assert (self.backend != None)
            return await self.backend.send_change_request(
                "/api/nodes/{}".format(str(node_id)),
                "PATCH",
                {
                    "properties": ujson.dumps({
                        "global": {
                            "title": str(title),
                        }
                    })
                }
            )
        except Exception as e:
            print("Ошибка: ", e)

    async def node_change_style_color(self, node_id, color):
        """
        Меняет заголовок узла
        :return: статус ответа и текст ответа
        """
        try:
            assert (self.backend != None)
            return await self.backend.send_change_request(
                "/api/nodes/{}".format(str(node_id)),
                "PATCH",
                {
                    "properties": ujson.dumps({
                        "style": {
                            "color": str(color),
                        }
                    })
                }
            )
        except Exception as e:
            print("Ошибка: ", e)

    async def node_create_child(self, map_id, parent_id, position, title):
        """
        Создает новый узел
        :param map_id: id карты
        :param parent_id: id узла предка
        :param position: номер позиции в списке потомков
        :param title: заголовок узла
        :return:
        """
        try:
            assert (self.backend != None)
            return await self.backend.send_change_request(
                "/api/nodes",
                "POST",
                {
                    "position": ujson.dumps(["P", position]),
                    "map_id": str(map_id),
                    "properties": ujson.dumps({
                        "style": {
                        },
                        "byType": {

                        },
                        "global": {
                            "title": title,
                        }
                    }),
                    "parent": str(parent_id),
                }
            )
        except Exception as e:
            print("Ошибка: ", e)

    async def node_delete(self, node_id):
        """
        Удаляет узел с указаным id
        :param node_id: id удаляемого узла
        :return: response
        """
        try:
            assert (self.backend != None)
            return await self.backend.send_change_request(
                "/api/nodes/{}".format(node_id),
                "DELETE",
                None
            )
        except Exception as e:
            print("Ошибка: ", e)

    async def node_user_property_create(self, node_id, type, title, value, visible=True):
        """
        Создает пользовательское свойство в узле
        :param node_id: id узла
        :param type: тип свойства
        :param title: заголовок свойства
        :param value:значение свойства
        :param visible: видимое или нет
        :return: response
        """
        try:
            assert (self.backend != None)
            return await self.backend.send_change_request(
                "/api/nodes/{}".format(node_id),
                "PATCH",
                {
                    "properties": ujson.dumps({
                        "byUser": [
                            {
                                "key": str(title),
                                "type_id": type,
                                "visible": visible,
                                "value": str(value),
                            }
                        ]
                    })
                }
            )
        except Exception as e:
            print("Ошибка: ", e)

    async def node_type_property_change(self, node_id, title, value):
        """
        Создает типовое свойство в узле
        :param node_id: id узла
        :param title: заголовок свойства
        :param value:значение свойства
        :return: response status, text
        """
        try:
            assert (self.backend != None)
            return await self.backend.send_change_request(
                "/api/nodes/{}".format(node_id),
                "PATCH",
                {
                    "properties": ujson.dumps({
                        "byType": {
                            "{}".format(title) :  value,
                        }
                    })
                }
            )
        except Exception as e:
            print("Ошибка: ", e)



    async def map_create(self, title, layout="LR"):
        """
        Создает карту и возвращает информацию о ней
        :param title: заголовок карты
        :param layout: тип расположения узлов
        :return:
        body:{
            "id":"", id карты
            "root_node_id": "", id корневого узла карты
            "owner": "", id владельца
            "layout": "", тип расположения узлов
            "name": "", заголовок карты
        }
        """
        response = await self.backend.send_change_request(
            "/api/maps",
            "POST",
            body={
                "name": title,
                "layout": layout,
            }
        )
        assert response[0] == 200

        body = ujson.loads(response[1])
        return body

    async def map_type_create(self, title, map_id):
        """
        Создает новый тип узлов с заголовком title для карты с id - map_id
        :param title: заголовок
        :param map_id: id карты
        :return: результат
        """
        response = await self.backend.send_change_request(
            "/api/node_types",
            "POST",
            body={
                "name": title,
                "map_id": map_id,
            }
        )
        try:
            assert response[0] == 200
        except AssertionError:
            print("Code: ", response[0])
            print("Message: ", ujson.loads(response[1]))
            return {}

        body = ujson.loads(response[1])
        return body

    async def map_type_icon_change(self, type_id, icon_url):
        """
        Изменить иконку у узла type_id на icon_url
        :param type_id:  id узла
        :param icon_url: url иконки (строка)
        :return: результат
        """
        response = await self.backend.send_change_request(
            f"/api/node_types/{type_id}",
            "PATCH",
            body={
                "icon": icon_url,
            }
        )
        try:
            assert response[0] == 200
        except AssertionError:
            print("Code: ", response[0])
            print("Message: ", ujson.loads(response[1]))
            return {}

        body = ujson.loads(response[1])
        return body

    async def map_type_delete(self, type_id):
        """
        Удаляет тип
        :param type_id: id типа
        :return:
        """
        response = await self.backend.send_change_request(
            f"/api/node_types/{type_id}",
            "DELETE",
            body={
            }
        )
        try:
            assert response[0] == 200
        except AssertionError:
            print("Code: ", response[0])
            print("Message: ", ujson.loads(response[1]))
            return {}
        return response[0]

    async def map_type_property_create(self, type_id, property_type,  name, multivalued=False, displayable=False, default_value="", as_icon=False, icons={}, position=0):
        """
        Создает свойство у типа
        :param type_id: id типа, не путать с property_type
        :param property_type: id типа данных для узла
        :param name: название типа
        :param multivalued: 1 или несколько значений
        :param displayable: отображать в узле?
        :param default_value: значение по-умолчанию
        :param as_icon: с иконкой или без
        :param icons: список иконок
        :param position: положение в списке свойств типа
        :return:
        """

        response = await self.backend.send_change_request(
            f"/api/node_types/{type_id}/properties",
            "POST",
            body={
                "owner_id": type_id,
                "name": name,
                "type_id": property_type,
                "multivalued": multivalued,
                "displayable": displayable,
                "default_value": default_value,
                "as_icon": as_icon,
                "icons": ujson.dumps(icons),
                "position": position,
                }
        )
        try:
            assert response[0] == 200
        except AssertionError:
            print("Code: ", response[0])
            print("Message: ", ujson.loads(response[1]))
            return {}

        body = ujson.loads(response[1])
        return body

    async def map_user_add(self, map_id, username, access='', send_mail=False):
        """
        Добавить пользователя username на карту map_id с правами access(если пусто, то только чтение, админ - admin)
        :param map_id:  id карты
        :param username: имя пользователя
        :param access: права пользователя на карте
        :param send_mail: отправить уведомление
        :return: пользователь
        """
        if access!='':
            access = { "map": access }                     
        else:
            access = {}
        response = await self.backend.send_change_request(
            f"/api/maps/{map_id}/users",
            "POST",
            body={
                "username": username,
                "access": access,
                "send_mail": send_mail,
            }
        )
        try:
            assert response[0] == 200
        except AssertionError:
            print("Code: ", response[0])
            print("Message: ", ujson.loads(response[1]))
            return {}

        body = ujson.loads(response[1])
        return body

    async def map_user_delete(self, map_id, user_id):
        """
        Удаляет пользователя user_id с карты map_id
        :param map_id:  id карты
        :param user_id: id пользователя        
        :return:
        """        
        response = await self.backend.send_change_request(
            f"/api/maps/{map_id}/users",
            "DELETE",
            body={
                "user_id": user_id,
            }
        )
        try:
            assert response[0] == 200
        except AssertionError:
            print("Code: ", response[0])
            print("Message: ", ujson.loads(response[1]))
            return {}

        return response[0]
